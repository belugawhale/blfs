import socket

def Main(raw_host, raw_port, username):
    host = str(raw_host)
    port = int(raw_port)
    if port > 65535:
        return "PORT NUMBER TOO BIG"

    s = socket.socket()
    s.connect((host, port))
    s.send(username)
    
    filename = raw_input("Filename: ")
    
    if filename != 'q':
        s.send(filename)
        data = s.recv(1024)
        if data[:6] == 'EXISTS':
            filesize = long(data[6:])
            msg = raw_input("File exists, " + str(filesize) +"Bytes, download? (Y/N): ")
            if msg == "Y" or "y":
                s.send("OK")
                tbdf = open('files/'+filename, 'wb')
                data = s.recv(1024)
                totalRecv = len(data)
                tbdf.write(data)
                while totalRecv < filesize:
                    data = s.recv(1024)
                    totalRecv += len(data)
                    tbdf.write(data)
                    print "{0:.2f}".format((totalRecv/float(filesize))*100)+ "% Done"
                print "Download Complete!"
                tbdf.close()
        else:
            print "File Does Not Exist!"

    s.close()
