import socket
import threading
import os

def RetrFile(name, sock):
    filename = "server_files/" + sock.recv(1024)
    if os.path.isfile(filename):
        sock.send("EXISTS " + str(os.path.getsize(filename)))
        userResponse = sock.recv(1024)
        if userResponse[:2] == 'OK':
            with open(filename, 'rb') as tbsf:
                bytesToSend = tbsf.read(1024)
                sock.send(bytesToSend)
                while bytesToSend != "":
                    bytesToSend = f.read(1024)
                    sock.send(bytesToSend)
    else:
        sock.send("ERR ")

    sock.close()

def Main(raw_host, raw_port, server_name):
    os.chdir("..")
    os.chdir("files/server/%s" % server_name)
    host = str(raw_host)
    port = int(raw_port)
    if port > 65535:
        return "PORT NUMBER TOO BIG"
    
    s = socket.socket()
    s.bind((host,port))
    s.listen(5)

    print "Server Started"
    while True:
        sk, addr = s.accept()
        usrnm = sk.recv(1024)
        print "%s Connected" % usrnm
        t = threading.Thread(target=RetrFile, args=("RetrThread", sk))
        t.start()
         
    s.close()

Main(sys.argv[1], sys.argv[2], sys.argv[3])
