import os
import subprocess
import core.client

def startServer(server_ip, server_port, server_name):
    global proclist
    proclist = []
    if os.name == 'nt':
        console = ['cmd.exe', '/c']
    else:
        print "OS not supported"
        return 1
    cmd = ["python", "core/server.py", server_ip, server_port, server_name]
    proc = subprocess.Popen(console + cmd)
    proclist.append(["%s" % server_name, proc])

def command():
    global proclist
    sip = "127.0.0.1"
    sp = 42080
    command = raw_input("Command: ")
    if command == "server":
        if command[:14] == "server create ":
            pass
        elif command[:13] == "server start ":
            startServer(sip, sp, command[13:])
        elif command[:12] == "server stop ":
            for process in proclist:
                if process[len(command[:12])+1:] == command[:12]:
                           proccess[1].kill()
                           proclist.remove(process)
        elif command[:14] == "server delete":
            pass
        elif command == "server add file":
            pass
        else:
            print "Server Command Arguments:\n"
            print "create <name> - Create Server"
            print "start <name> - Start Server"
            print "stop <name> - Stop Server"
            print "delete <name> - Delete Server"
            print "add file - Add File GUI"
